# Renewable Power Generation in Germany #

This repository contains code for a webservice, that queries market data from the smard.de website of Bundesnetzagentur 
and estimates the actual share of renewable energy in electricity production. 

You can send a get request to https://renewable-1609.herokuapp.com/ to receive a response like {"Share Renewables": 0.61, 
"Market Data": {"Photovoltaik": 4618, "Wind Onshore": 1214, "Wind Offshore": 683, "Sonstige Konventionelle": 265, "Pumpspeicher": 73, 
"Erdgas": 1814, "Steinkohle": 462, "Braunkohle": 1606, "Kernenergie": 985, "Sonstige Erneuerbare": 34, "Wasserkraft": 510, 
"Biomasse": 1117}} which means that the actual share of renewable electricity production is estimated at 61%.

If the market data is incomplete for more than 4 hours then no share is estimated. 
This is indicated by a null value for "Share Renewables" in the reponse of the webservice.
