from datetime import datetime, timedelta
from threading import Semaphore
from market_data import get_market_data, is_incomplete, is_renewable
import cherrypy
import os

response = None
response_timestamp = None
semaphore = Semaphore()


def share_renewables(market_data):
    if is_incomplete(market_data):
        return None
    total = 0.0
    renewable = 0.0
    for key, value in market_data.items():
        total += value
        if is_renewable(key):
            renewable += value
    return round(renewable / total, 2)


def get_response():
    global response, response_timestamp, semaphore
    now = datetime.now()
    if response is None or (now - response_timestamp) > timedelta(minutes=15):
        if semaphore.acquire(timeout=1):
            try:
                market_data = get_market_data(now)
                response =  {"Share Renewables": share_renewables(market_data), "Market Data": market_data}
                response_timestamp = now
            finally:
                semaphore.release()
    return response


class WebService(object):
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def index(self):
        return get_response()


cherrypy.config.update({'engine.autoreload.on': False,
                        'server.socket_host': '0.0.0.0',  # an jede IP binden
                        'server.socket_port': int(os.environ.get('PORT', 5000))})
cherrypy.quickstart(WebService())


