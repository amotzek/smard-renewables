import requests
import json
from xml.etree import ElementTree
from datetime import datetime

MODULE_IDS = [2000125, 2000123,
              1001224, 1004066, 1004067, 1004068,
              1001223, 1004069, 1004071, 1004070,
              1001226, 1001228, 1001227, 1001225]


def get_market_data(when, region="DE"):
    timestamp = int(when.timestamp() * 1000)
    parameters = {"format":"XML", "type":"discrete", "language":"de"}
    parameters["region"] = region
    parameters["moduleIds"] = MODULE_IDS
    parameters["timestamp_from"] = timestamp - 14400000
    parameters["timestamp_to"] = timestamp
    request_form = {"request_form": [parameters]}
    response = requests.post("https://www.smard.de/nip-download-manager/nip/download/market-data",
                             data=json.dumps(request_form))
    try:
        root = ElementTree.fromstring(response.text)
        timed_pairs = traverse_baustein(root)
        return latest_values(timed_pairs)
    except:
        return {}


def traverse_baustein(root):
    timed_pairs = []
    for element in root.iter("baustein"): # alle Elemente "baustein" irgendwo im Baum
        name = element.find("baustein_name").text # der Inhalt des ersten Unterelements "baustein_name"
        traverse_werte(element.find("werte"), name, timed_pairs) # das erste Unterelement "werte"
    return timed_pairs


def traverse_werte(parent, name, timed_pairs):
    for child in parent.findall("wert_detail"): # alle Unterelemente "wert_detail"
        wert = child.find("wert").text
        if wert != "-":
            datum = child.find("Datum").text
            uhrzeit = child.find("Uhrzeit").text
            timed_pair = {"key": name}
            timed_pair["timestamp"] = datetime.strptime(datum + " " + uhrzeit, "%d.%m.%Y %H:%M")
            timed_pair["value"] = int(wert.replace(".", ""))
            timed_pairs.append(timed_pair)


def latest_values(timed_pairs):
    sorted_timed_pairs = sorted(timed_pairs, key=lambda timed_pair: timed_pair["timestamp"])
    sorted_timed_pairs.reverse()
    value_by_key = {}
    for timed_pair in sorted_timed_pairs:
        key = timed_pair["key"]
        if key not in value_by_key:
            value_by_key[key] = timed_pair["value"]
    return value_by_key


def is_renewable(key):
    return key in ("Photovoltaik", "Wind Onshore", "Wind Offshore",
                   "Pumpspeicher", "Sonstige Erneuerbare", "Wasserkraft",
                   "Biomasse")


def is_incomplete(market_data):
    return len(market_data) < 11